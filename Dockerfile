FROM keymetrics/pm2:latest-alpine

# Bundle APP files
COPY app app/
COPY config/production.json5 config/default.json5
COPY package.json .
COPY ecosystem.config.js .

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install --production

# Expose the listening port of your app
EXPOSE 4000

CMD ["pm2-runtime", "start", "ecosystem.config.js" ]