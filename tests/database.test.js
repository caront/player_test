const { expect } = require('chai');

const SQLService = require('../app/services/database/database.service');

describe('SQL', () => {
    it('try to connect', async () => {
        let connection = await SQLService.GetConnection()
    })

    it('exec simple select', async () => {
        let connection = await SQLService.GetConnection();

        await connection.SingleQuery("SELECT * FROM players", [])
    })

    it('exec transaction', async () => {
        let connection = await SQLService.GetConnection();
        let transaction = await connection.BeginTransaction()
        await transaction.TransactedQuery("SELECT * FROM players", [])
        await transaction.EndTransaction()
    })
})
