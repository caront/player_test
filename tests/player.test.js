const { expect } = require('chai');
const Faker = require("faker");
const PlayerService = require('../app/services/players/player.service');
const Exception = require('../app/exceptions/player.exception');
const { TypeException } = require('../app/exceptions/type.exception');


const FIRSTNAME = Faker.name.firstName();
const LASTNAME = Faker.name.lastName()
const FIRSTNAME_UPDATE = Faker.name.firstName();

describe('Player', () => {
    it('Select', async () => {
        let player = await PlayerService.Select(1);

        expect(player).to.not.be.null;
    })

    it('Select execption', (done) => {
        PlayerService.Select(0).then(
            (player) => {
                done("error")
            },
            (ex) => {
                if (ex instanceof Exception.PlayerNotFound) {
                    done()
                }
                else
                    done(ex);
            }
        )

    })

    it('Insert', async () => {
        let player = await PlayerService.Insert(FIRSTNAME, LASTNAME);
        expect(player.firstname).to.be.equal(FIRSTNAME);
        expect(player.lastname).to.be.equal(LASTNAME)
    })

    it('Insert duplicate', (done) => {
        PlayerService.Insert(FIRSTNAME, LASTNAME).then(
            (res) => {
                done('error')
            },
            (ex) => {
                if (ex instanceof Exception.DuplicatePlayer)
                    done();
                else
                    done(ex);
            }
        )
    })

    it('Insert type exception', (done) => {
        PlayerService.Insert(null, 2).then(
            (res) => {
                done('error')
            },
            (ex) => {
                if (ex instanceof TypeException)
                    done();
                else
                    done(ex);
            }
        )
    })

    it('update', async () => {
        let player = await PlayerService.Select(2);
        await PlayerService.Update(player, FIRSTNAME_UPDATE, null);

        expect(player.firstname).to.be.equal(FIRSTNAME_UPDATE);
    })

    it('delete', async () => {

    })

    it('search', async () => {
        const result = await PlayerService.Search('', 1, 10, 'NAME_ASC');
    })

    it('search bad orderby', (done) => {
        PlayerService.Search('', 1, 10, 'NONE').then((e) => { done('error') }, (ex) => {
            if (ex instanceof Exception.InvalidOrderBy)
                done()
            else
                done(ex)
        })
    })


    it('exist true', async () => {
        const result = await PlayerService.Exist(1);
        expect(result).to.be.equal(true);
    })

    it('exist false', async () => {
        const result = await PlayerService.Exist(0);
        expect(result).to.be.equal(false);
    })

})
