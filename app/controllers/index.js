const Express = require('express');

var router = Express.Router();

router.get('/', (request, response) => {
    response.status(200).json({
        version : 1.0,
        name : 'Player'
    })
})

router.use('/players', require('./players.controller'));

module.exports = router;