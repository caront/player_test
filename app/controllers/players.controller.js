const Express = require('express');
const LOG = require('../utils/log');
const PlayerService = require('../services/players/player.service');
const PlayerException = require('../exceptions/player.exception');
const TypeException = require('../exceptions/type.exception');
const Validate = require('../middlewares/validate.middleware');
const Query = require('../middlewares/query.middleware');
const Schemas = require('../schemas/player.schema');


var router = Express.Router();


router.get('/', Query(Schemas.SEARCH_PLAYER), async (request, response) => {
    try {
        const result = await PlayerService.Search(
            request.query['q'],
            parseInt(request.query['page']),
            parseInt(request.query['itemPerPage']),
            request.query['sortBy']
        )
        response.status(200).json({
            result: result.json()
        })
    } catch (ex) {
        LOG.debug(`Exception catched : DELETE /:id : ${ex.message}`);
        response.status(500).json({
            message: "Internal error"
        })
    }
})

router.head('/:playerid', async (request, response) => {
    try {
        const exist = await PlayerService.Exist(parseInt(request.params['playerid']))
        response.status(exist ? 200 : 404).json({});
    } catch (ex) {
        LOG.debug(`Exception catched : HEAD /:id : ${ex.message}`);
        response.status(500).json({
            message: "Internal error"
        })
    }
})

router.param('id', async (request, response, next, id) => {
    try {
        const player = await PlayerService.Select(parseInt(id));
        request['player'] = player;
        next()
    } catch (ex) {
        LOG.debug(`Exception catched : PARAMS ID : ${ex.message}`);
        if (ex instanceof PlayerException.PlayerNotFound) {
            response.status(404).json({
                type: ex.type,
                message: ex.message,
            })
        }
        else {
            response.status(500).json({
                message: "Internal error"
            })
        }
    }
})

router.get('/:id', (request, response) => {
    try {
        const player = request.player;
        response.status(200).json({
            player: player.json()
        })
    } catch (ex) {
        LOG.debug(`Exception catched : GET /:id : ${ex.message}`);
        response.status(500).json({
            message: "Internal error"
        })
    }
})

router.post('/', Validate(Schemas.POST_PLAYER), async (request, response) => {
    try {
        const player = await PlayerService.Insert(request.body['firstname'], request.body['lastname']);
        response.status(201).json({
            player: player.json()
        })
    } catch (ex) {
        LOG.debug(`Exception catched : POST / : ${ex.message}`);
        if (ex instanceof PlayerException.DuplicatePlayer) {
            response.status(409).json({
                type: ex.type,
                message: ex.message,
                firstname: ex.firstname,
                lastname: ex.lastname
            })
        }
        else if (ex instanceof TypeException.TypeException){
            response.status(400).json({
                type: ex.type,
                message: ex.message,
            })
        }
        else
            response.status(500).json({
                message: "Internal error"
            })
    }
});

router.patch('/:id', Validate(Schemas.PATCH_PLAYER), async (request, response) => {
    try {
        const player = request.player;
        await PlayerService.Update(player, request.body['firstname'], request.body['lastname']);
        response.status(200).json({
            player: player.json()
        })
    } catch (ex) {
        LOG.debug(`Exception catched : PATCH /:id : ${ex.message}`);
        if (ex instanceof PlayerException.UpdateDuplicatePlayer) {
            response.status(409).json({
                type: ex.type,
                message: ex.message,
                firstname: ex.firstname,
                lastname: ex.lastname
            })
        }
        else if (ex instanceof TypeException.TypeException){
            response.status(400).json({
                type: ex.type,
                message: ex.message,
            })
        }
        else
            response.status(500).json({
                message: "Internal error"
            })
    }
});


router.delete('/:id', async (request, response) => {
    try {
        const player = request.player;
        await PlayerService.Delete(player);
        response.status(200).json({
            player: player.json()
        })
    } catch (ex) {
        LOG.debug(`Exception catched : DELETE /:id : ${ex.message}`);
        response.status(500).json({
            message: "Internal error"
        })
    }
})


module.exports = router;