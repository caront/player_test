const Log = require('../../utils/log');
const { Pool, createPool, PoolConfig } = require('mysql');
const { ConnectionException } = require('../../exceptions/database.exception');
const SqlConnection = require('./database.connection');
const Config = require('config');

class SqlService {

    constructor() {
        if (SqlService.instance) {
            throw new Error("Error - use SqlService.GetInstance()");
        }

        this._connectionPool = createPool({
            connectionLimit: Config.SQL.maxConnections,
            user: Config.SQL.username,
            password: Config.SQL.password,
            database: Config.SQL.database,
            port : Config.SQL.port,
            host: Config.SQL.host
        });
    }




    /**
     * Get a Sql Connection
     * 
     * @async
     * @return {Promise<SqlConnection>}
     * @throws {ConnectionException}
     */
    async GetConnection() {
        return new Promise((resolve) => {
            this._connectionPool.getConnection((error, connection) => {
                if (error)
                    throw new ConnectionException(error);
                resolve(new SqlConnection(connection));
            });
        });
    }
};


SqlService.instance = null;

function GetInstance() {

    if (!SqlService.instance) {
        SqlService.instance = new SqlService();
    }

    return SqlService.instance;
}


module.exports = GetInstance()
