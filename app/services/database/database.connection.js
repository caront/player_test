const { Connection } = require('mysql');
const Logs = require('../../utils/log');
const { SqlQueryException, SqlTransactionException } = require('../../exceptions/database.exception');

class SqlConnection {
    constructor(connection) {
        this._connection = connection;
        this._hasError = false;
    }

    async SingleQuery(query, parameters){
        return new Promise((resolve, reject) => {
            this._connection.query(query, parameters,
                (error, rows) => {
                    if (error)
                        reject(new SqlQueryException(error));
                    resolve(rows);
                });
        });
    }


    async BeginTransaction(){
        return new Promise((resolve, reject) => {
            this._connection.beginTransaction((error) => {
                if (error) {
                    reject(new SqlTransactionException(error));
                } else {
                    resolve(this);
                }
            });
        });
    }

    async TransactedQuery(query, parameters){
        return new Promise((resolve, reject) => {
            this._connection.query(query, parameters, (error, rows) => {
                if (error) {
                    this._hasError = true;
                    this._lastError = error;
                    reject(new SqlQueryException(error));
                } else {
                    resolve(rows);
                }
            });
        });

    }

    async EndTransaction(){
        return new Promise((resolve) => {
            if (this._hasError) {
                this._connection.rollback(() => {
                    resolve({})
                });
            } else {
                this._connection.commit(() => {
                    resolve({});
                });
            }
        })

    }
}

module.exports = SqlConnection
