const SQLService = require('../database/database.service');
const Exception = require('../../exceptions/player.exception');
const SQLException = require('../../exceptions/database.exception')
const Model = require('../../models/player.model');

async function Select(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const connection = await SQLService.GetConnection();

            const rows = await connection.SingleQuery('SELECT id, firstname, lastname, created_at, updated_at FROM players WHERE id = ?', [id]);
            if (rows.length === 0)
                throw new Exception.PlayerNotFound(id);
            resolve(Model.NewFromRaw(rows[0]));
        } catch (ex) {
            reject(ex);
        }
    })
}

async function Exist(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const connection = await SQLService.GetConnection();
            const rows = await connection.SingleQuery('SELECT id FROM players WHERE id = ?', [id]);
            resolve(rows.length !== 0);
        } catch (ex) {
            reject(ex);
        }
    })
}


async function Insert(firstname, lastname) {
    return new Promise(async (resolve, reject) => {
        try {
            const connection = await SQLService.GetConnection();
            const rows = await connection.SingleQuery('INSERT INTO players(`FIRSTNAME`, `LASTNAME`) VALUES (?,?)', [firstname, lastname]);
            resolve(new Model.Player(rows.insertId, firstname, lastname, new Date(), new Date))
        } catch (ex) {
            if (ex instanceof SQLException.SqlQueryException && ex.code == "ER_DUP_ENTRY") {
                reject(new Exception.DuplicatePlayer(firstname, lastname));
            }
            else
                reject(ex);
        }
    })
}

async function Update(id, firstname, lastname) {
    return new Promise(async (resolve, reject) => {
        try {
            const connection = await SQLService.GetConnection();
            const transaction = await connection.BeginTransaction();

            if (firstname !== undefined && firstname !== null && firstname !== '')
                await transaction.TransactedQuery('UPDATE players SET firstname = ? WHERE id = ?', [firstname, id])
            if (lastname !== undefined && lastname !== null && lastname !== '')
                await transaction.TransactedQuery('UPDATE players SET lastname = ? WHERE id = ?', [lastname, id])
            await transaction.EndTransaction();
            resolve();
        } catch (ex) {
            if (ex instanceof SQLException.SqlTransactionException && ex.code == "ER_DUP_ENTRY") {
                reject(new Exception.DuplicatePlayerOnUpdate(id, firstname, lastname));
            }
            else
                reject(ex);
        }
    })
}


const searchQueries = {
    'NAME_ASC': {
        query: 'SELECT SQL_CALC_FOUND_ROWS id, firstname, lastname, created_at, updated_at FROM players WHERE (UPPER(firstname) LIKE ? OR UPPER(lastname) LIKE ?) ORDER BY firstname ASC LIMIT ?, ?',
        params: (q, skip, limit) => {
            return [`%${q}%`, `%${q}%`, skip, limit]
        }
    },
    'NAME_DESC': {
        query: 'SELECT SQL_CALC_FOUND_ROWS id, firstname, lastname, created_at, updated_at FROM players WHERE (UPPER(firstname) LIKE ? OR UPPER(lastname) LIKE ?) ORDER BY firstname DESC LIMIT ?, ?',
        params: (q, skip, limit) => {
            return [`%${q}%`, `%${q}%`, skip, limit]
        }
    },
    'NEWEST': {
        query: 'SELECT SQL_CALC_FOUND_ROWS id, firstname, lastname, created_at, updated_at FROM players WHERE (UPPER(firstname) LIKE ? OR UPPER(lastname) LIKE ?) ORDER BY created_at ASC LIMIT ?, ?',
        params: (q, skip, limit) => {
            return [`%${q}%`, `%${q}%`, skip, limit]
        }
    },
    'OLDEST': {
        query: 'SELECT SQL_CALC_FOUND_ROWS id, firstname, lastname, created_at, updated_at FROM players WHERE (UPPER(firstname) LIKE ? OR UPPER(lastname) LIKE ?) ORDER BY created_at DESC LIMIT ?, ?',
        params: (q, skip, limit) => {
            return [`%${q}%`, `%${q}%`, skip, limit]
        }
    }
}

async function Search(q, skip, limit, orderBy) {
    return new Promise(async (resolve, reject) => {
        try {
            if (searchQueries[orderBy] === undefined)
                throw new Exception.InvalidOrderBy(orderBy)

            const connection = await SQLService.GetConnection();
            const transaction = await connection.BeginTransaction();

            const query = searchQueries[orderBy].query;
            const params = searchQueries[orderBy].params(q, skip, limit);

            let rows = await transaction.TransactedQuery(query, params);
            let totals = await transaction.TransactedQuery('SELECT FOUND_ROWS() AS total', []);

            if (rows.length != 0)
                rows[0]['total_item'] = totals[0].total;
            await transaction.EndTransaction();
            resolve(rows);

        } catch (ex) {
            reject(ex);
        }
    })
}

async function Delete(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const connection = await SQLService.GetConnection();

            await connection.SingleQuery('DELETE FROM players WHERE `ID` = ?', [id]);
            resolve();
        } catch (ex) {
            reject(ex);
        }
    })
}


module.exports = {
    Select,
    Exist,
    Insert,
    Update,
    Search,
    Delete
}