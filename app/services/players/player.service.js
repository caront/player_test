const DAO = require('./player.dao');
const Types = require('../../utils/type');
const PlayerSearchResult = require('../../search/player.search');
const Player = require('../../models/player.model');

async function Select(id) {
    return DAO.Select(id);
}

async function Insert(firstname, lastname) {
    let error = [];
    Types.checktype('firstname', firstname, Types.StringType, 'string', false, false, error);
    Types.checktype('lastname', lastname, Types.StringType, 'string', false, false, error);
    Types.Summary(error);

    return DAO.Insert(firstname, lastname);
}

async function Update(player, firstname, lastname) {
    let error = [];
    Types.checktype('firstname', firstname, Types.StrignTypeNullOrUndefined, 'string', true, true, error);
    Types.checktype('lastname', lastname, Types.StrignTypeNullOrUndefined, 'string', true, true, error);
    Types.Summary(error);

    await DAO.Update(player.id, firstname, lastname);

    player.firstname = (firstname !== undefined && firstname !== null && firstname !== '') ? firstname : player.firstname;
    player.lastname = (lastname !== undefined && lastname !== null && lastname !== '') ? lastname : player.lastname;
    player.lastUpdate = new Date()

    return (player);
}

async function Delete(player) {

}

async function Exist(id) {
    return DAO.Exist(id)
}

async function Search(q, page, itemPerPage, sortBy) {
    let searchResult = new PlayerSearchResult(q, page, itemPerPage, sortBy);
    let result = await DAO.Search(searchResult.query.q, searchResult.skip, searchResult.limit, searchResult.query.sortBy);
    searchResult.setItems(result, Player.NewFromRaw);
    return Promise.resolve(searchResult);

}


async function Delete(player){
    return DAO.Delete(player.id);
}

module.exports = {
    Select,
    Insert,
    Update,
    Delete,
    Exist,
    Search
}