const Exception = require('../exceptions/type.exception')

function StringType(e) {
    return typeof e === typeof 'String'
}

function StrignTypeNullOrUndefined(e) {
    return e === undefined || e === null || typeof e === typeof 'String'
}

function checktype(fieldName, value, checkerFunction, type, canBeNUll, canBeUndefined, error) {
    if (!canBeUndefined && value === undefined)
        error.push({ field: fieldName, value: value, kind: 'undefined' });
    if (!canBeNUll && value === null)
        error.push({ field: fieldName, value: value, kind: 'null' });
    if (!checkerFunction(value)) {
        error.push({ field: fieldName, value: value, expect: type });
    }
}

function Summary(error) {
    if (!(error.length == 0))
        throw new Exception.TypeException(error);
}


module.exports = {
    checktype,
    StringType,
    Summary,
    StrignTypeNullOrUndefined
}