const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const Config = require('config');
const time = new Date();
const formater = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}]:[${level.toUpperCase()}]: ${message}`;
});

const logger = createLogger({
    format: combine(
        label({ label: 'Player' }),
        timestamp(),
        formater
    )
})


if (Config.Log.file) {
    const logPath = `${Config.Log.path}/log_${time.getFullYear()}_${time.getMonth()}_${time.getMonth()}__${time.getHours()}_${time.getMinutes()}.log`
    logger.add(new transports.File({
        prettyPrint: false,
        level: 'info',
        silent: false,
        timestamp: true,
        filename: logPath,
        maxsize: 40000,
        maxFiles: 10,
        json: false
    }));
}
if (Config.Log.console) {
    logger.add(new transports.Console({
        level: 'info',
        prettyPrint: true,
        colorize: true,
        silent: false,
        timestamp: true
    }));
}


module.exports = logger;
