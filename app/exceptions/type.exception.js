const BaseException = require('./base.exception');


class TypeException extends BaseException {
    constructor(errors){
        super('TYPE_ERRORS', 'some fields havnt the right type');
        this.errors = errors;
    }
}

module.exports = {
    TypeException
}