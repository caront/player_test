/**
 * Base exception
 * @class
 */
class BaseException extends Error {
    /**
     * @constructor
     * @param {string} type 
     * @param {string} message 
     */
    constructor(type, message) {
        super(message);
        this.type = type;
    }
}


module.exports = BaseException