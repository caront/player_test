const BaseException = require('./base.exception')

class ConnectionException extends BaseException {
    constructor(error) {
        super('CONNECTION_ERROR', error.message);
    }
}

class SqlQueryException extends BaseException {
    constructor(error) {
        super('SQL_QUERY_EXCEPTION', error.message);
        this.code = error['code'];
        this.sqlMessage = error['sqlMessage'];
        this.query = error['sql'];
        this.sqlState = error['sqlState'];
    }
}

class SqlTransactionException extends BaseException {
    constructor(error) {
        super('SQL_TRANSACTION_EXCEPTION', error.message);
        this.code = error['code'];
        this.sqlMessage = error['sqlMessage'];
        this.query = error['sql'];
        this.sqlState = error['sqlState'];
    }
}

module.exports = {
    ConnectionException,
    SqlTransactionException,
    SqlQueryException
}