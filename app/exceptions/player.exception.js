const BaseException = require('./base.exception');


class PlayerNotFound extends BaseException {
    constructor(id){
        super('PLAYER_NOT_FOUND', `No player found for id #${id}`);
        this.id = id;
    }
}

class DuplicatePlayer extends BaseException{
    constructor(firstname, lastname){
        super('DUPLICATE_PLAYER', `player with firstname : ${firstname}, lastname ${lastname} already exist on base`);
        this.firstname = firstname;
        this.lastname = lastname;
    }
}

class UpdateDuplicatePlayer extends BaseException{
    constructor(id, firstname, lastname){
        super('UPDATE_DUPLICATE_PLAYER', `player #${id}, with firstname : ${firstname}, lastname ${lastname} already exist on base`);
        this.firstname = firstname;
        this.lastname = lastname;
    }
}

class InvalidOrderBy extends BaseException {
    constructor(orderBy){
        super('INVALID_ORDERBY', `invalid orderBy : ${orderBy}`)
        this.orderBy = orderBy;
    }
}

module.exports = {
    PlayerNotFound,
    DuplicatePlayer,
    UpdateDuplicatePlayer,
    InvalidOrderBy
}