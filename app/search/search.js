
/**
 * @class SearchResult
 */
class SearchResult {
    /**
     * @constructor
     * @param {string} type research type
     * @param {number} page 
     * @param {number} itemPerPage 
     * @param {any} query 
     */
    constructor(type, page, itemPerPage, query) {
        this.page = page;
        this.type = type;
        this.itemPerPage = itemPerPage;
        this.query = query;
        this.items = [];
        this.skip = (page - 1) * itemPerPage;
        this.limit = this.skip + itemPerPage;
    }



    setItems(data, resultParse) {
        try {
            this.items = data ? data.map((res) => { return resultParse(res) }) : [];
            this.total = data && data.lenght === 0 ? 0 : data[0].total_item;
        }
        catch (ex) {
            this.total = 0
        }
    }


    /**
     * return jsonify result
     * @return {any}
     */
    json() {
        return {
            type: this.type,
            page: {
                page: this.page,
                maxPage: (this.total == 0 ? 0 : parseInt(String((this.total + this.itemPerPage - 1) / this.itemPerPage))),
                itemPerPage: this.itemPerPage,
                total: this.total,
            },
            query: this.query,
            items: this.items
        }
    }
}



module.exports = SearchResult