const SearchResult = require('./search');


class PlayerSearchResult extends SearchResult {
    /**
     * @constructor
     * @param {string} q 
     * @param {number} page 
     * @param {number} itemPerPage 
     * @param {string} sortBy can be name, modified, -name -modified
     */
    constructor(q, page, itemPerPage, sortBy) {
        super('PLAYERS', page, itemPerPage, {
            q,
            sortBy
        })
    }
}


module.exports = PlayerSearchResult