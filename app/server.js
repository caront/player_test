const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const Log = require('./utils/log');


const logger = function (req, res, next) {
    if (req.method != 'OPTIONS') {
        Log.info(`${req.method} -> ${req.url}`)
    }
    next();
}

var app = express();

app.use(logger);
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


module.exports = app;
