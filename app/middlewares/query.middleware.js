function query(queryValue) {
    return function (request, response, next) {
        for (let idx in queryValue) {
            if (request.query[idx] === undefined || request.query[idx] == '')
                request.query[idx] = queryValue[idx];
        }
        next();
    }
}

module.exports = query;