const Joi = require('joi')

function validate(schema) {
    return function (request, response, next,) {
        const result = Joi.validate(request.body, schema);

        if (result.error != null) {
            response.status(400).json({
                message: "invalid or missing parameters",
                details: result.error.details,
                parameters: request.body
            })
        }
        else {
            next();
        }
    }
}

module.exports = validate;
