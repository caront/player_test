const Log = require('./utils/log')
const Server = require('./server');
const Config = require('config').Server;

Log.info('starting server');

process.on('unhandledRejection', error => {
    Log.error('unhandledRejection ' + error.message);
});


Server.use('/api', require('./controllers'));

Server.listen(Config.port, (err) => {
    if (err) {
        Log.error(`Server not runnig, cause : ${err.message}`);
    }
    else {
        Log.info(`Server running on ${Config.port}`);
    }
})
