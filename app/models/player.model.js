
class Player {
    constructor(id, firstname, lastname, createdAt, lastUpdate){
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.createdAt = createdAt;
        this.lastUpdate = lastUpdate;
    }


    json(){
        return {
            id : this.id,
            firstname : this.firstname,
            lastname : this.lastname,
            createdAt : this.createdAt.getTime(),
            lastUpdate: this.lastUpdate.getTime()
        }
    }
}


function NewFromRaw(raw){
    return new Player(parseInt(raw.id), raw.firstname, raw.lastname, new Date(raw.created_at), new Date(raw.updated_at))
}


module.exports = {
    Player,
    NewFromRaw
}