const Joi = require('joi');


const POST_PLAYER = Joi.object().keys({
    firstname : Joi.string(),
    lastname: Joi.string()
})

const PATCH_PLAYER = Joi.object().keys({
    firstname : Joi.string().allow(null).allow('').optional(),
    lastname: Joi.string().allow(null).allow('').optional()
})

const SEARCH_PLAYER = {
    q : "",
    page : 1,
    itemPerPage : 10,
    sortBy : "NAME_ASC"
}

module.exports = {
    POST_PLAYER,
    PATCH_PLAYER,
    SEARCH_PLAYER
}